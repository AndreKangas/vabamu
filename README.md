# Please remove this readme if you are making a new project from blank kiosk.
Kiosk is a basic webpage used to run our programs for museums and every other light- or midrange interactive installations.
Kiosk is launch usually with chrome (or chromium in raspberrys). It has already several things built in for usage. Such as:
* websocket - for connecting to websocket server
* page system - we call them views in html, they are ment for setting up different phases of the project.

The hierarchy of the files are as followed:
```bash
root folder
├── css              # Here are stored the style files
├── js               # Here are all the librarys and javascript files
│   ├── main.js      # Here is the main JS code. To here you will add your own code as well.
│   └── test.js      # Here you can have the tests to make sure the code in main.js is running properly
├── errorlog.txt     # Here are stored the errors that the browser has run into
├── index.html       # Root file; from here all the other files are called into action
├── README.md
└── writelog.php    # PHP script that writes the browsers errors to errorlog.txt
```

## What we have in main.js by default

### Websocket
First we have already set up everything to connect to a websocketserver running on the machine locally.
Code is listening to a websocket server on ws://127.0.0.1:8181
If websocket receives a message it is passed to OnMessage function as a string. Then if the message is supposed to be a JSON object
you can easily convert it with:
```javascript
    var jsonData = JSON.parse(evt.data);
```
And now you can use everyhing that the websocket sent you.
Full example:
```javascript
    /* data that websocket will recieve is:
   {"action": "foo", "value": "bar"}
*/
function onMessage(evt){
    try{
        var json = JSON.parse(evt.data);
        if(json.action === "foo") {
            console.log(json.value) // console writes -> bar
        }
    }catch(Ex){
        console.log(Ex);
    }
}
```
if you wish to send data back to websocket write line as such:
```javascript
    // bar is a variable assigned earlier
    send(JSON.stringify({ action: "foo", value: bar }));
```

> if you don't wish to use websocket remove start function call from main.js line 16.


### Views
Views are like different phases or pages of the project. You can prebuild all the views in index.html between divs with view class assigned to it.
In main.js we have a function named goToView where you need to pass an integar ( 1-... ) of which view you want to use. This will add a "show" class to that view and remove all the other "show" classes.

homeView function will go to the first view.