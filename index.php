<?php 
if(isset($_GET['compile']) && $_GET['compile'] == '1') {
    ob_start();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

	<title>vabamu</title>
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/docs/assets/owlcarousel/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" href="OwlCarousel2-2.3.4/docs/assets/owlcarousel/assets/owl.theme.default.min.css"/>
	<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
    <link rel="stylesheet" href="css/font/stylesheet.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css?ver=<?= filemtime('css/style.css') ?>" />
    
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-touch-punch.js"></script>

    <script type="text/javascript" src="OwlCarousel2-2.3.4/docs/assets/owlcarousel/owl.carousel.min.js"></script>

    <script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/tests.js"></script>

	<!-- <script type="text/javascript">
		window.onerror = function (evt) {
			setTimeout(function () {
				window.location.reload();
			}, 5000);
			var dt = new Date();
			$.ajax({
				type: "POST",
				url: "writelog.php",
				data: { error: evt, datetime: dt }
			});
		}
	</script> -->
</head>

<body>
	<div class="view show">
		<div id="time-selection">
			<div class="container fluid">
				<p class="title-one main" data-est="3 KÜMNENDIT MOODI EESTIS" data-rus="ТРИ ДЕСЯТИЛЕТИЯ МОДЫ В ЭСТОНИИ">3 KÜMNENDIT MOODI EESTIS</p>
				<p class="title-two main" data-est="vali periood" data-rus="выбери период">vali periood</p>
			</div>

			<div class="container-fluid">
				<div class="row">
				  	<a class="toggle_selection col-sm time-one d-flex align-items-center justify-content-center" href="#" data-selection="1">
					<div class="aastad-one">
						<p data-est="ÜLEMINEKU-" data-rus="ПЕРЕХОДНОЕ">ÜLEMINEKU-</p>
						<p class="text-center" data-est="AASTAD" data-rus="ВРЕМЯ">AASTAD</p>
					</div>
				  	</a>
				  	<a class="toggle_selection col-sm time-two d-flex align-items-center justify-content-center" href="#" data-selection="2">
					  	<div class="aastad-two" data-est="1990-NDAD" data-rus="1990-ые">
						  	1990-NDAD
					  	</div>
				  	</a>
				  	<a class="toggle_selection col-sm time-three d-flex align-items-center justify-content-center" href="#" data-selection="3">
						<div class="aastad-three" data-est="2000-NDAD" data-rus="2000-ые">
							2000-NDAD
						</div> 
				  	</a>
				  	<a class="toggle_selection col-sm time-four d-flex align-items-center justify-content-center" href="#" data-selection="4">
					  	<div class="aastad-four" data-est="2010-NDAD" data-rus="2010-ые">
						  2010-NDAD
					  	</div>
				  	</a>
				</div>
			</div>

            <div class="text-center p-0 langImageWrap">
				<img class="lang-image" src="img/lang-button.png" alt="" language="est">
				<img class="lang-image invisible" src="img/lang-button.png" alt="" language="rus">
			</div>
			<div class="text-center p-2 langWrap">
				<a class="btn btn-default active lang" href="#" language='est'>EST</a>
				<a class="btn btn-default lang" href="#" language='rus'>RUS</a>
			</div>
		</div>


        <div class="clothing-selection container-fluid first-container d-none" data-selection-id="1">
            <div class="row puppet-row">
                <a class="toggle_selection col-1 tagasi-box boxes-one d-flex d-none align-items-center justify-content-center" href="#" data-selection="1">
                    <div class="tagasi-kiri" data-est="TAGASI" data-rus="НАЗАД">
                        TAGASI
                    </div>
                </a>
                <div class="col-10">
					<div class="puppet-container">
						<?= file_get_contents("img\period1\period1.svg")?>
					</div>
                    
                </div>
                <a class="col-1 valmis-box boxes-one d-flex d-none align-items-center justify-content-center" href="#">
                    <div class="valmis-kiri" data-est="VALMIS" data-rus="ГОТОВО">
                        VALMIS
                    </div>
                </a>
            </div>

            <div id="" class="row select-clothes">
                <div class="carousel crsl-one">
                    <div class="track">
                        <div class="nonloop owl-carousel">
                             <?php
                                $clothes_array = array(
                                'img/period1/period1-alakeha-1.png',
                                'img/period1/period1-alakeha-2.png',
                                'img/period1/period1-kogukeha-1.png',
                                'img/period1/period1-kogukeha-2.png',
                                'img/period1/period1-jalad-1.png',
                                'img/period1/period1-jalad-2.png',
                                'img/period1/period1-pea-1.png',
                                'img/period1/period1-pea-2.png',
                                'img/period1/period1-ülakeha-1.png',
                                'img/period1/period1-ülakeha-2.png',
                                'img/period1/period1-käsi-1.png',
                                'img/period1/period1-käsi-2.png',
                                'img/period1/period1-käsi-3.png'
                            );
                            ?>
                            <?php foreach($clothes_array as $key => $img): ?>
                                <div class="item">
                                    <img class="toggle_clothing" src="<?= $img ?>" alt="" data-counter="<?= $key + 1 ?>">
                                </div>
                            <?php endforeach; ?>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div id="" class="row d-none justify-content-center result fixed-bottom">
                <p class="col-12 pb-2" data-est="On 1988. aasta 11. september, ilm on septembri kohta mõnusalt
					soe, seega ei pea kartma, et sul nendes riietes külm hakkaks." data-rus="На календаре – 11 сентября 1988 года, погода для сентября достаточно теплая, поэтому не стоит бояться, что ты в этой одежде замерзнешь.».">On 1988. aasta 11. september, ilm on septembri kohta mõnusalt
						soe, seega ei pea kartma, et sul nendes riietes külm hakkaks. 
				</p>
                <p class="col-12 pb-4" data-est="Pealegi sinna, kuhu sina nüüd lähed, läheb veel vähemalt 100 000 inimest ja nii külg külje kõrval seistes ei saakski sul külm kontidesse pugeda. Eriti kui süda ärevusest taob ja ainuke asi, mis sulle
					täna külmavärinad võib tekitada, on muusika – veel viimane pilk peeglisse ja juba oled teel Eestimaa Laulule." data-rus="К тому же туда, куда ты отправляешься, придет еще как минимум 100 000 человек,
					с которыми ты будешь стоять плечом к плечу, так что холод в любом случае не сможет пробрать тебя. Особенно когда сердце бьется от волнения, а мурашки по коже могут побежать только из-за музыки – последний взгляд в зеркало, и ты идешь на «Песнь Эстонии».">Pealegi sinna, kuhu sina nüüd lähed, läheb veel vähemalt 100 000 inimest ja nii külg külje kõrval seistes ei saakski sul külm kontidesse pugeda. Eriti kui süda ärevusest taob ja ainuke asi, mis sulle
						täna külmavärinad võib tekitada, on muusika – veel viimane pilk peeglisse ja juba oled teel Eestimaa Laulule. 
				</p>
                <a class="algus-box boxes-algus-one d-flex btn btn-default btn-lg btn-block btn-choose justify-content-center" href="#">
                    <div class="algus-kiri col-md-auto" data-est="ALGUSESSE" data-rus="В НАЧАЛО">
                        ALGUSESSE
                    </div>
                </a>
            </div>
        </div>	


        <div class="clothing-selection container-fluid second-container d-none" data-selection-id="2">
            <div class="row puppet-row">
                <a class="toggle_selection col-1 tagasi-box boxes-two d-flex d-none align-items-center justify-content-center" href="#" data-selection="2">
                    <div class="tagasi-kiri" data-est="TAGASI" data-rus="НАЗАД">
                        TAGASI
                    </div>
                </a>
                <div class="col-10">
                    <div class="puppet-container">
						<?= file_get_contents("img\period2\period2.svg")?>
                    </div>
                </div>
                <a class="col-1 valmis-box boxes-two d-flex d-none align-items-center justify-content-center" href="#">
                    <div class="valmis-kiri" data-est="VALMIS" data-rus="ГОТОВО">
                        VALMIS
                    </div>
                </a>
            </div>

            <div id="" class="row select-clothes">
                <div class="carousel crsl-two">
                    <div class="track">
                        <div class="nonloop owl-carousel">
                            <?php
                                $clothes_array = array(
                                    'img/period2/period2-kogukeha-2.png',
                                    'img/period2/period2-jalad-2.png',
                                    'img/period2/period2-alakeha-1.png',
                                    'img/period2/period2-kogukeha-1.png',
                                    'img/period2/period2-kogukeha-3.png',
                                    'img/period2/period2-jalad-1.png',
                                    'img/period2/period2-pea-1.png',
                                    'img/period2/period2-pea-2.png',
                                    'img/period2/period2-ülakeha-1.png',
                                    'img/period2/period2-käsi-1.png',
                                    'img/period2/period2-käsi-2.png',
                                    'img/period2/period2-käsi-3.png'
                                );
                            ?>
                            <?php foreach($clothes_array as $key => $img): ?>
                                <div class="item">
                                    <img class="toggle_clothing" src="<?= $img ?>" alt="" data-counter="<?= $key + 1 ?>">
                                </div>
                            <?php endforeach; ?>
                        </div>
					    
                    </div>
                </div>
            </div>
            <div id="" class="row d-none justify-content-center result fixed-bottom">
            <p class="col-12 h4" data-est="Väga hea valik! Sa näed välja nagu sada krooni või lihtsalt öeldes
                üks koidula." data-rus="Отличный выбор! Ты выглядишь на все сто, как Лидия Койдула на стокроновой купюре!">Väga hea valik! Sa näed välja nagu sada krooni või lihtsalt öeldes
                üks koidula.
            </p>
            <p class="col-12 h4" data-est="Nüüd polegi muud kui kiiresti-kiiresti peatusesse, kus sind juba ootab veidi väsinud kollane lõõtsaga Ikarus, mis viib sind siiski vapralt kohale Pirita TOPi, kus täna astuvad lavale Must Q ja Nancy. Sul on täpselt teada ja viimseni lihvitud tantsusammud.
                Igaks juhuks proovid veel kord enne kodust väljumist – astud vasaku jala harki, tood parema selle kõrvale ja samal ajal pöörad end nii umbes 45° ja siis ruttu sama ka teisele poole ja nii ikka uuesti ja uuesti. Aga nüüd uksest välja ja ära unusta bussis oma
                piletit mulgustamast!" data-rus="А теперь – бегом на остановку, где тебя ждет видавший виды желтый «Икарус» с гармошкой, который тем не менее лихо довезет тебя до Pirita
                TOP, где сегодня на сцену выйдут Must Q и Nancy.
                Ты хорошо знаешь все танцевальные движения, отточенные до совершенства. На всякий случай еще раз репетируешь их напоследок перед выходом из дома: отводишь левую ногу, подтягиваешь к ней правую, поворачиваясь при этом на 45 градусов, а потом так же, но с другой
                ноги, повторяя это движение снова и снова. А теперь – марш за дверь и не забудь прокомпостировать в автобусе свой билетик!">Nüüd polegi muud kui kiiresti-kiiresti peatusesse, kus sind juba ootab veidi väsinud kollane lõõtsaga Ikarus, mis viib sind siiski vapralt kohale Pirita TOPi, kus täna astuvad lavale Must Q ja Nancy. Sul on täpselt teada ja viimseni lihvitud tantsusammud.
                Igaks juhuks proovid veel kord enne kodust väljumist – astud vasaku jala harki, tood parema selle kõrvale ja samal ajal pöörad end nii umbes 45° ja siis ruttu sama ka teisele poole ja nii ikka uuesti ja uuesti. Aga nüüd uksest välja ja ära unusta bussis oma
                piletit mulgustamast! 
            </p>
                <a class="algus-box boxes-algus-two d-flex btn btn-default btn-lg btn-block btn-choose justify-content-center" href="#">
                    <div class="algus-kiri col-md-auto" data-est="ALGUSESSE" data-rus="В НАЧАЛО">
                        ALGUSESSE
                    </div>
                </a>
            </div>
            
        </div>

        <div class="clothing-selection container-fluid third-container d-none" data-selection-id="3">
            <div class="row puppet-row">
                <a class="toggle_selection col-1 tagasi-box boxes-three d-flex d-none align-items-center justify-content-center" href="#" data-selection="3">
                    <div class="tagasi-kiri" data-est="TAGASI" data-rus="НАЗАД">
                        TAGASI
                    </div>
                </a>
                <div class="col-10">
                    <div class="puppet-container">
                        <?= file_get_contents("img\period3\period3.svg")?>
                    </div>
                </div>
                <a class="col-1 valmis-box boxes-three d-flex d-none align-items-center justify-content-center" href="#">
                    <div class="valmis-kiri" data-est="VALMIS" data-rus="ГОТОВО">
                        VALMIS
                    </div>
                </a>
            </div>

            <div id="" class="row select-clothes">
                <div class="carousel crsl-three">
                    <div class="track">
                        <div class="nonloop owl-carousel">
                            <?php
                        $clothes_array = array(
                            'img/period3/period3-jalad-1.png',
                            'img/period3/period3-alakeha-1.png',
                            'img/period3/period3-alakeha-2.png',
							'img/period3/period3-kogukeha-1.png',
							'img/period3/period3-jalad-2.png',
							'img/period3/period3-pea-1.png',
							'img/period3/period3-pea-2.png',
							'img/period3/period3-ülakeha-1.png',
							'img/period3/period3-ülakeha-2.png',
							'img/period3/period3-ülakeha-3.png',
							'img/period3/period3-käed-1.png',
							'img/period3/period3-käed-2.png',
                        );
                        ?>
                        <?php foreach($clothes_array as $key => $img): ?>
                            <div class="item">
                                <img class="toggle_clothing" src="<?= $img ?>" alt="" data-counter="<?= $key + 1 ?>">
                            </div>
                        <?php endforeach; ?>
                        </div>
                    
                    </div>
                </div>
            </div>
            <div id="" class="row d-none justify-content-center result fixed-bottom">
                <p class="col-12" data-est="Cool valik!" data-rus="Классный выбор!"> Cool valik!</p>
                <p class="col-12" data-est="
					Üks on kindel – sellise trenditeadlikkusega ei ole sul küll põhjust ajakirja Stiina muutumismängu kandideerida. Haarad veel kiiresti käigupealt kaasa oma CD-pleieri. Ega sul pole ju aega raisata – just täna avatakse uus uhke kaubanduskeskus, kuhu sa oma sõpradega
					suundud. Ainult et suure kiirustamisega unustasid oma pleieris patareisid vahetada ja see tähendab, et jalutades sind täna taustamuusika ei saada. Aga pole lugu, sul polegi nii väga pikk tee minna ja neid patareisid saab veel edukalt äratuskellas vähemalt pool
					aastat kasutada." data-rus="С такой осведомленностью в модных трендах у тебя нет причин подавать заявку на участие в конкурсе журнала Stiina
					на преображение. Быстро хватай на ходу свой CD-плеер, ведь время не терпит: сегодня открывается новый шикарный торговый центр, в который ты пойдешь с друзьями. Но
					в этой спешке ты забываешь поменять в плеере батарейки, а это значит, что во время прогулки тебя не будет фоном сопровождать музыка. Но ничего страшного – путь не длинный, а эти батарейки можно будет еще как минимум полгода использовать в будильнике.">
						Üks on kindel – sellise trenditeadlikkusega ei ole sul küll põhjust ajakirja Stiina muutumismängu kandideerida. Haarad veel kiiresti käigupealt kaasa oma CD-pleieri. Ega sul pole ju aega raisata – just täna avatakse uus uhke kaubanduskeskus, kuhu sa oma sõpradega
						suundud. Ainult et suure kiirustamisega unustasid oma pleieris patareisid vahetada ja see tähendab, et jalutades sind täna taustamuusika ei saada. Aga pole lugu, sul polegi nii väga pikk tee minna ja neid patareisid saab veel edukalt äratuskellas vähemalt pool
						aastat kasutada. 
				</p>
                

                <a class="algus-box boxes-algus-three d-flex btn btn-default btn-lg btn-block btn-choose justify-content-center" href="#">
                    <div class="algus-kiri col-md-auto" data-est="ALGUSESSE" data-rus="В НАЧАЛО">
                        ALGUSESSE
                    </div>
                </a>
            </div>
        </div>

        <div class="clothing-selection container-fluid fourth-container d-none" data-selection-id="4">
            <div class="row puppet-row">
                <a class="toggle_selection col-1 tagasi-box boxes-four d-flex d-none align-items-center justify-content-center" href="#" data-selection="4">
                    <div class="tagasi-kiri" data-est="TAGASI" data-rus="НАЗАД">
                        TAGASI
                    </div>
                </a>
                <div class="col-10">
                    <div class="puppet-container">
                        <?= file_get_contents("img\period4\period4.svg")?>
                    </div>
                </div>
                <a class="col-1 valmis-box boxes-four d-flex d-none align-items-center justify-content-center" href="#">
                    <div class="valmis-kiri" data-est="VALMIS" data-rus="ГОТОВО">
                        VALMIS
                    </div>
                </a>
            </div>

            <div id="" class="row select-clothes">
                <div class="carousel crsl-four">
                    <div class="track">
                        <div class="nonloop owl-carousel">
                            <?php
                        $clothes_array = array(
                            'img/period4/period4-alakeha-1.png',
                            'img/period4/period4-alakeha-2.png',
                            'img/period4/period4-jalad-1.png',
							'img/period4/period4-jalad-2.png',
							'img/period4/period4-jalad-3.png',
                            'img/period4/period4-ülakeha-2.png',
							'img/period4/period4-ülakeha-1.png',
							'img/period4/period4-kogukeha-1.png',
							'img/period4/period4-kogukeha-2.png',
							'img/period4/period4-käsi-3.png',
							'img/period4/period4-pea-1.png',
							'img/period4/period4-pea-2.png',						
							'img/period4/period4-käsi-1.png',
                        );
                        ?>
                        <?php foreach($clothes_array as $key => $img): ?>
                            <div class="item">
                                <img class="toggle_clothing" src="<?= $img ?>" alt="" data-counter="<?= $key + 1 ?>">
                            </div>
                        <?php endforeach; ?>
                        </div>
                    
                    </div>
                </div>
            </div>
            <div id="" class="row d-none justify-content-center result fixed-bottom">
                <p class="col-12" data-est="Sa näed välja suurepärane ja mis peamine – sa näed välja, nagu
                    sa ei peaks oma hea välimuse pärast vähimalgi määral pingutama. Stiil lihtsalt kuulub sinuga kokku, käib sundimatult sinuga kaasas." data-rus="Ты отлично выглядишь, а главное – ты выглядишь так, будто тебе не нужно прикладывать к этому никаких усилий. «Стиль» – твое второе имя, он всегда непринужденно сопровождает тебя.">Sa näed välja suurepärane ja mis peamine – sa näed välja, nagu
                    sa ei peaks oma hea välimuse pärast vähimalgi määral pingutama. Stiil lihtsalt kuulub sinuga kokku, käib sundimatult sinuga kaasas.
				</p>
                <p class="col-12" data-est="Telefon on ka kaasas, seega muud polegi vaja – kogu su elu mahub sinna sisse. Astud uksest välja, lähed lähima Bolti tõuksini
                    ja juba vuradki sõpradega kohtuma uude käsitööburgereid pakkuvasse söögikohta ja ainus, mis su sujuvat kulgemist takistab, on lugematu arv äärekivisid, millest peab kuidagi üles ja alla saama.  " data-rus="Телефон с собой, а больше ничего и
                    не надо: вся твоя жизнь помещается в нем. Выходишь за дверь, идешь до ближайшего самоката Bolt – и вот ты уже мчишься на встречу с друзьями в новом крафтовом кафе,
                    где делают отличные бургеры. И единственное, что мешает твоему плавному движению, это несчетное количество бордюров, через которые постоянно приходится переезжать.">Telefon on ka kaasas, seega muud polegi vaja – kogu su elu mahub sinna sisse. Astud uksest välja, lähed lähima Bolti tõuksini
                    ja juba vuradki sõpradega kohtuma uude käsitööburgereid pakkuvasse söögikohta ja ainus, mis su sujuvat kulgemist takistab, on lugematu arv äärekivisid, millest peab kuidagi üles ja alla saama.
				</p>
                <a class="algus-box boxes-algus-four d-flex btn btn-default btn-lg btn-block btn-choose justify-content-center" href="#">
                    <div class="algus-kiri col-md-auto" data-est="ALGUSESSE" data-rus="В НАЧАЛО">
                        ALGUSESSE
                    </div>
                </a>
            </div>
        </div>
	</div>
</body>
</html>
<?php
if(isset($_GET['compile']) && $_GET['compile'] == '1') {
    $recordedHtml = ob_get_clean();
    file_put_contents('index.html', $recordedHtml);
}
?>