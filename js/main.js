// var websocket;
// var wsAttempts = 10;
// var wsAttempt = 0;

// var resetTimeout = null;
// var resetTimeoutSeconds = 60 * 1000;

// $(function () {
// 	$(window).bind("click touchstart", function () {
// 		clearTimeout(resetTimeout);
// 		resetTimeout = setTimeout(function () {
// 			homeView();
// 		}, resetTimeoutSeconds);
// 	});
// 	document.onkeydown = checkKey;
// 	start();
// });

// function checkKey(e) {

// }
// function start() {
// 	if (websocket == undefined) {
// 		setInterval(function () { checkWS(websocket) }, 5000);
// 	}
// 	try {
// 		websocket = new WebSocket('ws://127.0.0.1:8181/');
// 		websocket.onmessage = onMessage;
// 		websocket.onopen = onOpen;
// 		websocket.onclose = onClose;
// 		websocket.onerror = onClose;
// 	}
// 	catch (e) {
// 		console.log(e);
// 	}

// }

// function checkWS(websocket) {
// 	if (websocket.readyState != websocket.OPEN) {
// 		if (wsAttempt < wsAttempts) {
// 			websocket.close();
// 			start();
// 		}
// 		else {
// 			window.location.reload();
// 		}
// 		wsAttempt++;
// 	}
// }
// function onOpen(evt) {
// 	/*_d(evt);*/
// }
// function onClose(evt) {
// 	evt.target.close();
// }
// function sendAction(act) {
// 	send(JSON.stringify({ action: act }));
// }

// function send(data) {
// 	try {
// 		websocket.send(data);
// 	}
// 	catch (e) { }
// }
// function nextView(view) {
// 	if (view.next().filter('.view').length != 0) {
// 		$(".view").removeClass("show");
// 		view.next().filter('.view').addClass("show");
// 	}
// }
// function prevView(view) {
// 	if (view.prev().filter('.view').length != 0) {
// 		$(".view").removeClass("show");
// 		view.prev().filter('.view').addClass("show");
// 	}
// }
// function homeView() {
// 	$(".view").removeClass("show");
// 	$(".view:first-of-type").addClass("show");
// }
// function goToView(viewNumber) {
// 	$(".view").removeClass("show");
// 	$(".view:nth-of-type(" + viewNumber + ")").addClass("show");
// }
// function onMessage(evt) {
// 	try {
// 		console.log(evt);
// 	} catch (Ex) {
// 		console.log(Ex);
// 	}
// }
// $(document).ready(function () {
// 	//write your code here
// });

/* toggle between puppet clothes and slider items; toggle betweend selected clothes based on their category */

function initClothingSelection(){
	window.allowToggleClothing = false;

	$('body').on('click', '.toggle_clothing', function() {
        var id = $(this).data('counter');
        var selection = $(this).parents('.clothing-selection').data('selection-id');
		console.log(allowToggleClothing);
        if(allowToggleClothing) {
			var clothingSelection = $('.clothing-selection[data-selection-id="' + selection + '"] .puppet-container [data-clothing-id="' + selection + '-' + id + '"]');
			var category = clothingSelection.attr("data-category");
			$('.clothing-selection[data-selection-id="' + selection + '"] .puppet-container [data-category="' + category + '"]:not([data-clothing-id="' + selection + '-' + id + '"])').addClass("d-none");
			clothingSelection.toggleClass("d-none");
        }
    })


	/* activation of toggle clothes, depending the pointer, mouse or touch movement on screen by pixels allowed; */

	var trackElements = document.querySelectorAll('.item');
	trackElements.forEach((track) => {
		var initialPosition = null;
		var moving = false;
		var transform = 0;
		var gestureStart = (e)=> {
			allowToggleClothing = true;
			initialPosition = e.pageX;
			moving = true;
		};

		var gestureMove = (e)=> {
			if (moving) {
				var currentPosition = e.pageX;
				var diff = currentPosition - initialPosition;
				var transformX = transform + diff;
				if (transformX > 20) {
					allowToggleClothing = false;
				};
			}
		};

		var gestureEnd = (e)=> {
			moving = false;
		}
		if (window.PointerEvent) {
			track.addEventListener('pointerdown', gestureStart);
			track.addEventListener('pointermove', gestureMove);
			track.addEventListener('pointerup', gestureEnd);
		} else {
			track.addEventListener('touchdown', gestureStart);
			track.addEventListener('touchmove', gestureMove);
			track.addEventListener('touchup', gestureEnd);
			track.addEventListener('mousedown', gestureStart);
			track.addEventListener('mousemove', gestureMove);
			track.addEventListener('mouseup', gestureEnd);
		};
	})
		
}


/* selection of owl carousel jquery plugin which generates and customizes carousel */

function carousel(){
	var owl = $('.nonloop').owlCarousel({
		center: true,
		items:2,
		loop:false,
		margin:10,
		dots: false,
		responsive:{
			600:{
				items:4
			}
		}
	}); 
}


$(function(){
	initClothingSelection();
	initNavigation();
	languageSelection();
	carousel();

    $('body').on('click', '.toggle_selection', function(e) {
        e.preventDefault();
        var id = $(this).data('selection');
        
        $('.clothing-selection[data-selection-id="'+id+'"]').toggleClass("d-none");
		$('.puppet-container image:not(#nukk)').addClass("d-none");
    });
});


/* Reset owl carousel back to the beginning */

function resetOwlCarousels() {
	$('.nonloop').trigger('to.owl.carousel', [0, 0]);
}


/*Toggle between 2 main divs with buttons - activation with d-none; display: none */

function toggleSelection(id) {
	$('.clothing-selection div[data-selection-id="'+id+'"]').toggleClass("d-none");
	$("#time-selection").toggleClass("d-none");
}


/*Navigating through divs with buttons - activation with d-none; display: none; reset owl carousel */

function initNavigation(){

	$(".tagasi-box").click((e)=> {
		resetOwlCarousels();
	});

	$(".valmis-box").click((e)=> {
		$(".tagasi-box").removeClass("d-flex");
		$(".valmis-box").removeClass("d-flex");
		$(".select-clothes").addClass("d-none");
		$(".result").addClass("d-flex");
		$( ".puppet-container" ).parent().attr('class', 'col-12');
	});

	$(".algus-box").click((e)=> {
		$(".clothing-selection").addClass("d-none");
		$(".select-clothes").removeClass("d-none");
		$("#time-selection").removeClass("d-none");
		$(".result").removeClass("d-flex");
		$(".tagasi-box").addClass("d-flex");
		$(".valmis-box").addClass("d-flex");
		$( ".puppet-container" ).parent().attr('class', 'col-10');
		$('.puppet-container image:not(#nukk)').addClass("d-none");
		resetOwlCarousels();
	});
}


/* Language Selection, change language with attribute text and change the visibility of image above the lang */

function languageSelection(){
	var langEl = document.querySelector('.langWrap');
	var link = document.querySelectorAll('.lang');
	link.forEach(el => {
		el.addEventListener('click', () => {
			langEl.querySelector('.active').classList.remove('active');
			el.classList.add('active');
			var attr = el.getAttribute('language');
			document.querySelector('.lang-image[language="' + attr + '"]').classList.remove("invisible");
			document.querySelector('.lang-image:not([language="' + attr + '"])').classList.add("invisible");
			document.querySelectorAll('[data-' + attr + ']').forEach(el2 => {
				el2.textContent = el2.getAttribute("data-" + attr);
			});
		});
	});
}